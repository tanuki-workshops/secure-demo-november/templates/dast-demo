# web-app-pages 

The source code of the application is located at `./website`.
> if you change this, change the value of `WEBSITE_LOCATION` in `.gitlab-ci.yml`

```yaml
variables:
  WEBSITE_LOCATION: "website"
```

- The project visibility must be public
- The Pahes must be available for everyone
